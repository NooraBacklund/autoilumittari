﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Autoilumittari.Resources
{
    public class Car
    {
        public string Name { get; set; }
        public double BaseConsumption { get; set; }
        public string Attribs { get; set; }
        public bool Active { get; set; }

        public double GetConsumption(double speed, double distance)
        {
            return Math.Round(ConsumptionPer100km(speed) * (distance / 100.0), 2);
        }

        private double ConsumptionPer100km(double speed)
        {
            return BaseConsumption * Math.Pow(1.009, (speed - 1.0));
        }

        public double GetConsumptionPer100km(double speed)
        {
            return Math.Round(ConsumptionPer100km(speed), 2);
        }

        public static TimeSpan GetDriveTime(double speed, double distance)
        {
            if (speed != 0)
            {
                return TimeSpan.FromHours(distance / speed);
            }
            else
            {
                return new TimeSpan(0);
            }
        }
    }
}
