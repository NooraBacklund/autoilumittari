# Autoilumittari

## Käytetyt teknologiat

Sovellus on totettu [Blazor WebAssembly (WASM)](https://dotnet.microsoft.com/apps/aspnet/web-apps/blazor) 
-pohjaisena ratkaisuna. Blazor pohjautuu .NET ohjelmointikieleen, ja mahdollistaa WebAssemly-
pohjaisten verkkosivujen tarjoamisen selaimille. 

Käyttöliittymän muotoilussa ja responsiivisuudessa on hyödynnetty Bootstrapiä. 

## Asennusohjeet ja käyttö

### dotnet run

Huom. sovelluksen ajaminen alla olevien ohjeiden mukaan vaatii .NET 5.0 tai uudemman version. 

* Lataa lähdekoodi
* Avaa komentorivi/PowerShell
* Navigoi lähdekoodit sisältävään kansioon ("Autoilumittari/Autoilumittari")
* Suorita komento `dotnet run`
* Navigoi selaimella komentorivin tulosteessa näkyvään osoitteeseen (esim. http://localhost:5001)

### IIS

Sovellus on julkaistavissa IIS:in päälle, tosin tätä ei ole kehityksen yhteydessä erikseen testattu. 
Julkaisua varten tarvittava dokumentaatio on saatavilla 
[Microsoftin sivuilta](https://docs.microsoft.com/en-us/aspnet/core/blazor/host-and-deploy/webassembly?view=aspnetcore-5.0#iis).

### HerokuApp

Sovellus on toistaiseksi käytettävissä [HerokuApps -sivustolla](https://young-thicket-44605.herokuapp.com/). 
Paljon omaa käsialaani parempi ohjeistus sovelluksen julkaisemisesta Herokuun on saatavilla 
[Swimburgerin sivustolta](https://swimburger.net/blog/dotnet/how-to-deploy-blazor-webassembly-to-heroku). 

## Kuvaus ratkaisusta

### Ulkoasu ja käyttö 

Ratkaisu on yhden sivun web-pohjainen sovellus, joka esittää sovelluksen käyttäjälle laskelmia 
polttoainekulutukseen sekä ajankäyttöön liittyen käyttäjän antamien syötteiden mukaan. 

Sovelluksen yläreunassa on valittavissa kolme eri etukäteen määritettyä autoa (A, B ja C), joita
klikkaamalla aktivoi aina kyseisen auton mukaiset laskelmat. Autojen alapuolella on syötekentät 
kahdelle eri nopeudelle, sekä kuljettavalle etäisyydelle. Nämä tiedot syöttämällä kunkin auton alle 
tuotetaan laskelmat autojen kulutuksista eri nopeuksilla, sekä matka-ajat. Valitun auton tarkemmat
polttoaineen kulutukseen liittyvät eroavaisuudet sekä matkaan käytetyn ajan ero tulostetaan ruudun
alareunaan. 

Laskennat päivittyvät käyttäjän muuttaessa syötettyjä tietoja tai käyttäjän valitessa uuden auton. 
Erillinen "submit"-painike ei ollut tarpeen. 

Nopeussyötteet ovat mielivaltaisesti rajoitettu välille 0-300 km/h, etäisyys välille 0-30000 km. 
Syötteet ovat rajoitettu numeerisiin arvoihin. 

### Tekninen toteutus

Sovellukseen toteutettiin `ConsumptionCalculator`-komponentti, jonka tehtävänä on tuottaa sivuston
käyttäjälle polttoaineen kulutukseen ja ajankäyttöön liittyvään laskentaan tarvittavat toiminnot. 
Käyttäjälle esitettävät autot (`Car`-objektit) ovat kovakoodattu, ja alustetaan uudelleen sivun latauksen
yhteydessä. 

`Car`-objektit sisältävät tiedon auton nimestä ja peruskulutuksesta, sekä muutaman visuaalista 
esitystä helpottavan muuttujan (aktiivisuus ja attribuutit). Polttoainekulutus saadaan `Car`-olioiden
metodeilla, ajoaika luokan staattisella funktiolla `GetDriveTime`. 

Autojen polttoainetietojen esittämiseen hyödynnettiin `ConsumptionDisplay`-komponenttia, jolle
annettiin parametereinä nopeus, etäisyys ja auto, jonka tietojen pohjalta laskelmat tehdään. 
Komponentti huolehti laskelmien tekemisestä ja tiedon esittämisestä oikealla ulkoasulla. 

`EditForm` ja `DataAnnotationsValidator` -kombinaatiolla hallittiin nopeuksiin ja etäisyyteen liittyvät
käyttäjän syötteet. `EditForm` tarjosi tarvittavat työkalut (`InputNumber`, `@bind-Value=...`)
perustason syötteiden tarkistukseen ja muuttujiin liittämiseen. `DataAnnotationsValidator` mahdollisti
syötteen tyyppiä tarkemmat validointisäännöt ja ilmoitukset. `@bind-Value` ja `onchanged` yhteensopimattomuuden
johdosta Blazorin `ShouldRender()` funktioon piti lisätä datan oikeellisuuden tarkistus, etteivät 
laskentakomponentti ja kulutuskomponentit esittäisi laskelmiaan mahdottomasta datasta (kuten negatiivisista
etäisyyksistä) huolimatta. 

`ConsumptionComparison`-komponentti visualisoi sille parametreinä annetut polttoaineen ja ajankäytön
eroavaisuudet. 